package br.com.itau.api.y9.cadastrofundossistemaslegados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiY9CadastroFundosSistemasLegadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiY9CadastroFundosSistemasLegadosApplication.class, args);
	}

}
