package br.com.itau.api.y9.cadastrofundossistemaslegados.controllers;

import br.com.itau.api.y9.cadastrofundossistemaslegados.models.CadastroFundo;
import br.com.itau.api.y9.cadastrofundossistemaslegados.producer.CadastroFundoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CadastroFundoController {

    @Autowired
    private CadastroFundoProducer cadastroFundoProducer;

    @PostMapping
    public void create(@RequestBody CadastroFundo cadastroFundo){
        cadastroFundoProducer.enviarAoKafka(cadastroFundo);
    }
}
