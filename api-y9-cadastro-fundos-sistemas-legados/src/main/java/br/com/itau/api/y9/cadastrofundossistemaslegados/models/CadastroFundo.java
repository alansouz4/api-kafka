package br.com.itau.api.y9.cadastrofundossistemaslegados.models;

import lombok.Data;

@Data
public class CadastroFundo {

    private Long idAtivo;
    private Long idProduto;
    private String nomeFundo;
}
