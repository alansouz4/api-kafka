package br.com.itau.api.y9.cadastrofundossistemaslegados.producer;

import br.com.itau.api.y9.cadastrofundossistemaslegados.models.CadastroFundo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CadastroFundoProducer {

    @Autowired
    private KafkaTemplate<String, CadastroFundo> producer;

    public void enviarAoKafka(CadastroFundo cadastroFundo){
        producer.send("cadastro-fundo", cadastroFundo);
    }
}
