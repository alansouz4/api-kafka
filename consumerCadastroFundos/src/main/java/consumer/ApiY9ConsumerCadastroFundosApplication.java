package consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiY9ConsumerCadastroFundosApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiY9ConsumerCadastroFundosApplication.class, args);
    }
}
