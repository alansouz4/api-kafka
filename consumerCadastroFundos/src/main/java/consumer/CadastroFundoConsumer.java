package consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import producer.CadastroFundo;

@Component
public class CadastroFundoConsumer {


    @KafkaListener(topics = "cadastro-fundo", groupId = "group-id")
    public void receber(@Payload CadastroFundo cadastroFundo) {
        System.out.println("Recebi um cadastro do fundo: "
                + cadastroFundo.getNomeFundo());
    }
}
