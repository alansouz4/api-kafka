package producer;

import lombok.Data;

@Data
public class CadastroFundo {

    private Long idAtivo;
    private Long idProduto;
    private String nomeFundo;
}
